const express = require('express');
const app = express();
const port = 8000;
const Caesar = require('caesar-salad').Caesar;

app.get('/', (req, res) => {
  res.send('Enter password');
});

app.get('/encode/:password', (req, res) => {
  const password = Caesar.Cipher('c').crypt(req.params.password);
  res.send(password);
});

app.get('/decode/:password', (req, res) => {
  const password = Caesar.Decipher('c').crypt(req.params.password);
  res.send(password);
});

app.listen(port, () => {
  console.log('We are on port ' + port);
});
