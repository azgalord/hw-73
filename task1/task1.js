const express = require('express');
const app = express();
const port = 8000;

app.get('/', (req, res) => {
  res.send('Hello to my mirroring server!');
});

app.get('/:word', (req, res) => {
  res.send('<h1>' + req.params.word + '</h1>');
});

app.listen(port, () => {
  console.log('We are on port ' + port);
});
